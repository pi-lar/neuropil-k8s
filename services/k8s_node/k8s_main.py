#!/usr/bin/env python3

import os
from k8s_node import KubernetesNode
from k8s_args import human_identifier, port, hostname


def main():

    np_node001 = KubernetesNode(human_identifier, port, hostname)
    np_node001.start_node()
    np_node001.print_address()

    node_join_to = os.getenv("NODE_JOIN_TO")

    if node_join_to:
        np_node001.join_node(node_join_to)

    try:
        while True:
            np_node001.run(0)
    except KeyboardInterrupt:
        np_node001.shutdown()


main()
