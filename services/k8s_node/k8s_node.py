import sys
import logging
from neuropil import NeuropilNode, np_token

logging.basicConfig(
    level=logging.DEBUG,
    stream=sys.stdout,  # Print the Logs to the cli
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
)

_logger = logging.getLogger(__file__)


class KubernetesNode(NeuropilNode):
    """Contains methods to Use a Neuropil Node"""

    def __init__(self, human_identifier: int, port: int, hostname: str):
        super().__init__(
            port,
            hostname,
            auto_run=False,
            n_threads=0,
        )
        self.human_identifier = human_identifier
        self.port = port
        self.hostname = hostname

    def authn_allow_all(self, node: NeuropilNode, token: np_token):
        """Bypasses the Authentication Process of Neuropil Node"""
        _logger.info(
            f"{self.get_fingerprint()}: authn: {token.subject} {token.get_fingerprint()}"
        )
        return True

    def authz_allow_all(self, node: NeuropilNode, token: np_token):
        """Bypasses the Authorization Process of Neuropil Node"""
        _logger.info(
            f"{self.get_fingerprint()}: authz: {token.subject} {token.get_fingerprint()}"
        )
        return True

    def acc_allow_all(self, node: NeuropilNode, token: np_token):
        """Bypasses the Accounting Process of Neuropil Node"""
        _logger.info(f"{self.get_fingerprint()}: acc: {token.subject}")
        return True

    def enableAAA(self):
        """Sets the AAA Callbacks to use the Bypassing methods"""
        self.set_authenticate_cb(self.authn_allow_all)
        self.set_authorize_cb(self.authz_allow_all)
        self.set_accounting_cb(self.acc_allow_all)

    def start_node(self):
        """Starts a Neuropil Node, runs it for short period of time and then prints a info message to the logger"""
        self.enableAAA()
        self.run(0)
        _logger.info(f"Neuropil Node {self.human_identifier} has been started")

    def join_node(self, connection_string):
        _logger.info(
            f"Neuropil Node {self.human_identifier} requests join with {connection_string}"
        )
        super().join(connection_string)

    def print_address(self):
        """Prints the Neuropil Node Address and Subject to the logger"""
        _logger.info(
            f"Neuropil Node {self.human_identifier} has the following Address and Subject: {self.get_address()}"
        )

    def shutdown(self):
        """A Method that should be able to shutdown the started node"""
        _logger.info(f"Neuropil Node {self.human_identifier} is going to shut down")
        super().shutdown(grace=False)
