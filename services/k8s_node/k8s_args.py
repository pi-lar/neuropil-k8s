import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument("--node-human-identifier",
                    type=str,
                    required=False,
                    help="env variable node_human_identifier")
parser.add_argument("--node-port",
                    type=int,
                    required=True,
                    help="env variable node_port")
parser.add_argument("--node-hostname",
                    type=str,
                    required=False,
                    help="env variable node_hostname")
args = parser.parse_args()

os.environ["NODE_HUMAN_IDENTIFIER"] = args.node_human_identifier
os.environ["NODE_PORT"] = str(args.node_port)
os.environ["NODE_HOSTNAME"] = args.node_hostname

human_identifier = os.getenv("NODE_HUMAN_IDENTIFIER")
port = int(os.getenv("NODE_PORT"))
hostname = os.getenv("NODE_HOSTNAME")
