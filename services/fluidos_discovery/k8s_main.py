#!/usr/bin/env python3

import asyncio
import os
import logging
import sys
from k8s_node import KubernetesNode
from k8s_args import human_identifier, port, hostname, proto, bootstrap_url
from k8s_fluidos_node_discovery import NodeDiscovery

logging.basicConfig(
    level=logging.DEBUG,
    stream=sys.stdout,  # Print the Logs to the cli
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
)

logger = logging.getLogger(__file__)


async def main():

    np_node001 = KubernetesNode(
        human_identifier=human_identifier, port=port, hostname=hostname, proto=proto
    )

    await np_node001.add_discovery(NodeDiscovery())

    np_node001.print_address()

    await np_node001.run(0.01)
    np_node001.join_node(bootstrap_url)

    try:
        while True:
            await np_node001.run(0.01)

    except KeyboardInterrupt:
        np_node001.shutdown()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    loop.close()
