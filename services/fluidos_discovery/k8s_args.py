import argparse
import os
import socket

parser = argparse.ArgumentParser()
parser.add_argument(
    "--node-human-identifier",
    type=str,
    required=False,
    help="env variable node_human_identifier",
    default="0",
)
parser.add_argument(
    "--node-port",
    type=int,
    required=False,
    help="env variable node_port",
    default="31415",
)
parser.add_argument(
    "--node-hostname",
    type=str,
    required=False,
    help="env variable node_hostname",
    default="localhost",
)
parser.add_argument(
    "--node-bootstrap-url",
    type=str,
    required=False,
    help="env variable bootstrap_url",
    default="*:udp4:demo.neuropil.io:3400",
)
parser.add_argument(
    "--node-proto",
    type=str,
    required=False,
    help="env variable node protocol",
    default="pas4",
)
args = parser.parse_args()

human_identifier = str(os.getenv("NODE_HUMAN_IDENTIFIER", args.node_human_identifier))
proto = str(os.getenv("NODE_PROTO", args.node_proto))
port = int(os.getenv("NODE_PORT", args.node_port))
hostname = str(os.getenv("NODE_HOSTNAME", args.node_hostname))
bootstrap_url = str(os.getenv("NP_BOOTSTRAP_URL", args.node_bootstrap_url))
enable_flavor_discovery = os.getenv("FLAVOR_DISCOVERY_ENABLED", False)
