#!/usr/bin/env python3

import asyncio
from datetime import datetime, timedelta
import logging
from random import random

# from k8s_args import bootstrap_address
from kubernetes_asyncio import client, config
from kubernetes_asyncio.client.api_client import ApiClient
from kubernetes_asyncio.client.exceptions import ApiException
from discovery import DiscoveryHandler
from k8s_node import KubernetesNode
from neuropil import (
    NeuropilNode,
    neuropil,
    np_message,
    np_subject,
    np_token,
    NeuropilException,
)
from rear_models.node_identity import NodeIdentity
from rear_models.flavor import Flavor

import json


logger = logging.getLogger(__file__)

# neuropil subject tags
fluidos_flavor_discovery_v01 = (
    "urn:eu:fluidos:flavor:discovery:v0.1"  # hash xor'ed with node fingerprint
)
# neuropil data model tags
fluidos_node_flavor_v01 = "eu:fluidos:node:flavor:v0.1"


class FlavorDiscovery(DiscoveryHandler):

    task_queue = None

    def __init__(self, node_identity: NodeIdentity, role: str):
        self.fluidos_node_identity: NodeIdentity = node_identity
        self.role = role
        self.own_nodes = []
        self.last_update = datetime.now()

    @property
    def name(self):
        return f"{self.__class__.__name__}::{self.fluidos_node_identity.NodeID}"

    @classmethod
    def receive_fluidos_flavor(cls, node: NeuropilNode, message: np_message):
        # TODO: insert data into the flavor CRDS of Fluidos
        logger.info(
            f"{str(node.get_fingerprint())[:8]}... received flavor_data from {str(getattr(message, 'from'))} :: {message.raw()}"
        )
        try:
            flavor_data = Flavor.model_validate(json.loads(message.raw()))
            FlavorDiscovery.task_queue.put_nowait(
                cls.update_fluidos_flavor_crds(flavor_data)
            )
        except NeuropilException as ne:
            logger.info(f"neuropil error while processing received flavor data: {ne}")
        except Exception as e:
            logger.info(f"general error while processing received flavor data: {e}")
        return True

    @classmethod
    def authorize_fluidos_flavors(cls, node: NeuropilNode, token: np_token):
        # two cases to handle
        # a) our node is a provider of flavors. The consumer explicitly subscribed to our peronalized (Xor'ed) message subject. Just send over flovor data, as this might be a customer
        # b) our node is the consumer of flavors. The application logic already decided to subscribe to the personalized (Xor'ed) message subject.
        # in conclusion for both cases: just authorize the peer ...
        logger.info(
            f"{str(node.get_fingerprint())[:8]}... authorizing flavor exchange from {token.get_fingerprint()}"
        )
        return True

    async def setup(self, np_node: KubernetesNode):
        self.np_node = np_node
        FlavorDiscovery.task_queue = np_node.task_queue

        await config.load_config()

        # create the xor'ed subject out of "urn:eu:fluidos:flavor:discovery" and the fingerprint of the node
        flavor_discovery_subject = np_subject.generate(
            self.fluidos_node_identity.NodeID
        )
        flavor_discovery_subject.add(fluidos_flavor_discovery_v01)

        np_node.add_authorize_cb(
            flavor_discovery_subject, FlavorDiscovery.authorize_fluidos_flavors
        )
        mxp = self.np_node.get_mx_properties(flavor_discovery_subject)

        if self.role == "provider":
            mxp.audience_type = (
                neuropil.NP_MX_AUD_PUBLIC
            )  # should be neuropil.NP_MX_AUD_PRIVATE
            mxp.ackmode = neuropil.NP_MX_ACK_NONE
            mxp.role = neuropil.NP_MX_PROVIDER
            mxp.intent_ttl = 600  # the fluidos node lifetime (needs to be adjusted for the real use case)
            mxp.intent_update_after = 60  # refresh the fluidos flavour every minute
            mxp.message_ttl = 30
            mxp.max_retry = 0
            mxp.apply()

            FlavorDiscovery.task_queue.put_nowait(self.publish_fluidos_flavor())

        if self.role == "consumer":
            mxp.audience_type = (
                neuropil.NP_MX_AUD_PUBLIC
            )  # should be neuropil.NP_MX_AUD_PRIVATE
            mxp.ackmode = neuropil.NP_MX_ACK_NONE
            mxp.role = neuropil.NP_MX_CONSUMER
            mxp.intent_ttl = 600  # the fluidos node lifetime (needs to be adjusted for the real use case)
            mxp.intent_update_after = 60  # refresh the fluidos flavour every minute
            mxp.message_ttl = 30
            mxp.max_retry = 0
            mxp.apply()

            # add receive callback
            self.np_node.set_receive_cb(
                flavor_discovery_subject, FlavorDiscovery.receive_fluidos_flavor
            )

        logger.info("initialized fluidos flavor identity discovery channel")

    async def _get_own_flavors(self, k8s_node: str) -> list[Flavor]:

        own_flavors = []
        async with ApiClient() as api:
            try:
                co_api = client.CustomObjectsApi(api)
                flavor_objects = await co_api.list_namespaced_custom_object(
                    group="nodecore.fluidos.eu",
                    version="v1alpha1",
                    namespace="fluidos",
                    plural="flavors",
                )

                for flavor_obj in flavor_objects["items"]:
                    logger.info(f"reading {flavor_obj}")
                    flavor_obj = await co_api.get_namespaced_custom_object(
                        group="nodecore.fluidos.eu",
                        version="v1alpha1",
                        namespace="fluidos",
                        plural="flavors",
                        name=f"{flavor_obj['metadata']['name']}",
                    )
                    flavor = Flavor.model_validate(flavor_obj["spec"])
                    flavor.flavorID = flavor_obj["metadata"]["name"]
                    if flavor.owner.nodeID == k8s_node:
                        own_flavors.append(flavor)
                        logger.info(f"appending {flavor.flavorID} to sync list")

            except ApiException as api_e:
                logger.info(f"error ignored because fo automated retry: {api_e}")

        return own_flavors

    async def publish_fluidos_flavor(self):

        if self.last_update >= datetime.now() - timedelta(seconds=60):
            FlavorDiscovery.task_queue.put_nowait(self.publish_fluidos_flavor())
            return

        flavor_discovery_subject = np_subject.generate(
            self.fluidos_node_identity.NodeID
        )
        flavor_discovery_subject.add(fluidos_flavor_discovery_v01)

        if self.np_node.np_has_receiver_for(flavor_discovery_subject):
            flavor_data = await self._get_own_flavors(self.fluidos_node_identity.NodeID)
            for flavor in flavor_data:
                self.np_node.send(flavor_discovery_subject, flavor.model_dump_json())
                logger.info(
                    f"{self.fluidos_node_identity.NodeID} ... sending flavor data"
                )
        else:
            logger.info(
                f"{self.fluidos_node_identity.NodeID} ... has no consumer of flavor data"
            )
        self.last_update = datetime.now()
        # asyncio.get_running_loop().call_later(60, self.publish_fluidos_flavor())
        FlavorDiscovery.task_queue.put_nowait(self.publish_fluidos_flavor())

    @classmethod
    async def update_fluidos_flavor_crds(cls, fluidos_flavor_info: Flavor):

        logger.info(f"now processing flavor data {fluidos_flavor_info.flavorID}")

        # Define the group, version, and plural of the CRD
        group = "nodecore.fluidos.eu"  # Replace with your CRD group
        version = "v1alpha1"  # Replace with your CRD version
        plural = "flavors"  # Replace with your CRD plural
        namespace = "fluidos"

        async with ApiClient() as api:
            co_api = client.CustomObjectsApi(api)
            create_new = False
            flavor_k8s_obj = None
            try:
                flavor_k8s_obj = await co_api.get_namespaced_custom_object(
                    group=group,
                    version=version,
                    namespace=namespace,
                    plural=plural,
                    name=fluidos_flavor_info.flavorID,
                )
                flavor = Flavor.model_validate(flavor_k8s_obj["spec"])
                flavor.flavorID = flavor_k8s_obj["metadata"]["name"]
                # merge two dictionaries (overwrite existing values with new ones)
                flavor_k8s_obj["spec"] = (
                    flavor.model_dump() | fluidos_flavor_info.model_dump()
                )
                logger.info("patching existing crds known flavors  ...")

            except ApiException as api_e:
                if api_e.reason == "Not Found":
                    logger.info("creating new crd known cluster ...")
                    flavor_k8s_obj = {
                        "apiVersion": f"{group}/{version}",
                        "kind": "Flavor",  # Replace with your CRD kind
                        "metadata": {
                            "name": fluidos_flavor_info.flavorID,
                            "namespace": namespace,
                        },
                        "spec": fluidos_flavor_info.model_dump(),
                    }
                    create_new = True
                pass  # Debugger 12.1 bug crashes here without this statement

            try:
                if create_new:
                    await co_api.create_namespaced_custom_object(
                        group=group,
                        version=version,
                        namespace=namespace,
                        plural=plural,
                        body=flavor_k8s_obj,
                    )
                else:
                    await co_api.patch_namespaced_custom_object(
                        group=group,
                        version=version,
                        namespace=namespace,
                        plural=plural,
                        name=fluidos_flavor_info.flavorID,
                        body=flavor_k8s_obj,
                        _content_type="application/merge-patch+json",
                    )
                logger.info("crd known cluster written to k8s cluster...")
            except ApiException as api_e:
                logger.info(f"error while updating known cluster crd: {api_e}")

            pass  # Debugger 12.1 bug crashes here without this statement
