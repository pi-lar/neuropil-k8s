#!/usr/bin/env python3

import asyncio
from datetime import datetime, timedelta
import logging
import json

# from k8s_args import bootstrap_address
from kubernetes_asyncio import client, config
from kubernetes_asyncio.client.api_client import ApiClient
from kubernetes_asyncio.client.exceptions import ApiException
from discovery import DiscoveryHandler
from k8s_node import KubernetesNode
from neuropil import (
    NeuropilNode,
    neuropil,
    np_message,
    np_subject,
    np_token,
    NeuropilException,
)
from rear_models.node_identity import NodeIdentity

from k8s_fluidos_flavor_discovery import FlavorDiscovery
from fluidos_crds_models.known_cluster import AddressSpec, KnownCluster, Status

logger = logging.getLogger(__file__)

# neuropil subject tags
fluidos_node_discovery_v01 = "urn:eu:fluidos:node:discovery:v0.2"
# neuropil data model tags
fluidos_node_identity_v01 = "eu:fluidos:node:identity:v0.1"


class NodeDiscovery(DiscoveryHandler):

    task_queue: asyncio.Queue = None
    fluidos_node_id = None
    np_node = None

    def __init__(self):
        self.fluidos_node_identity = None
        self.task_queue = None
        self.countries = []
        self.cities = []
        self.latitudes = []
        self.longitudes = []

    @property
    def name(self):
        return self.__class__.__name__

    @classmethod
    def authorize_fluidos_nodes(cls, node: NeuropilNode, token: np_token):
        fluidos_node_info = None
        node_identity = None
        logger.info("authorizing fluidos node")
        try:
            fluidos_node_info = token.get_attr_bin(
                key=fluidos_node_identity_v01
            ).decode()

            node_identity = NodeIdentity.model_validate(json.loads(fluidos_node_info))

            if node_identity.NodeID != cls.fluidos_node_id:
                cls.task_queue.put_nowait(
                    cls.update_fluidos_known_clusters(node_identity)
                )
                cls.task_queue.put_nowait(
                    cls.np_node.add_discovery(
                        FlavorDiscovery(node_identity=node_identity, role="consumer")
                    )
                )
                logger.info(
                    f"authorization of external fluidos node {node_identity.NodeID} completed"
                )
            else:
                logger.info("received information about myself")

        except NeuropilException as ne:
            # ignore non existence of fluidos_type (aka a consumer of fluidos resources) and/or country
            pass
        except Exception as e:
            logger.info(f"unexpected error {e}")

        return True

    async def setup(self, np_node: KubernetesNode):

        NodeDiscovery.task_queue = np_node.task_queue
        NodeDiscovery.np_node = np_node

        await config.load_config()

        flavor_discovery_module = FlavorDiscovery(
            node_identity=self.fluidos_node_identity, role="provider"
        )

        have_fluidos_resources = False

        async with ApiClient() as api:
            configmap_fluidos_node = None
            try:
                v1 = client.CoreV1Api(api)
                configmap_fluidos_node = await v1.read_namespaced_config_map(
                    name="fluidos-node-identity", namespace="fluidos"
                )
                NodeDiscovery.fluidos_node_id = configmap_fluidos_node.data["nodeID"]
                # check whether we are a provider of kubernetes resources
                nodes = await v1.list_node(
                    label_selector="node-role.fluidos.eu/resources=true",
                )

                if len(nodes.items) > 0:
                    have_fluidos_resources = True
                    # extract information from flavors to use them as attribute filter
                    flavors = await flavor_discovery_module._get_own_flavors(
                        NodeDiscovery.fluidos_node_id
                    )
                    [
                        self.countries.append(flavor.location.country)
                        for flavor in flavors
                        if flavor.location.country not in self.countries
                    ]
                    [
                        self.cities.append(flavor.location.city)
                        for flavor in flavors
                        if flavor.location.city not in self.cities
                    ]
                    [
                        self.latitudes.append(flavor.location.latitude)
                        for flavor in flavors
                        if flavor.location.latitude not in self.latitudes
                    ]
                    [
                        self.longitudes.append(flavor.location.longitude)
                        for flavor in flavors
                        if flavor.location.longitude not in self.longitudes
                    ]

            except ApiException as e:
                logger.info(
                    f"could not read fluidos node identity, node is shutdown: {e}"
                )
                raise e

        self.fluidos_node_identity = NodeIdentity(
            Domain=configmap_fluidos_node.data["domain"],
            NodeID=configmap_fluidos_node.data["nodeID"],
            IP=configmap_fluidos_node.data["ip"],
            Port=configmap_fluidos_node.data["port"],
            # additionalInformation=addon_info,
        )
        flavor_discovery_module.fluidos_node_identity = self.fluidos_node_identity
        logger.info("loaded own fluidos node identity information")

        flavor_discovery_subject = np_subject.generate(fluidos_node_discovery_v01)

        mxp = np_node.get_mx_properties(flavor_discovery_subject)
        mxp.audience_type = neuropil.NP_MX_AUD_VIRTUAL
        mxp.role = (
            neuropil.NP_MX_PROSUMER
            if have_fluidos_resources
            else neuropil.NP_MX_CONSUMER
        )
        mxp.intent_ttl = 300
        mxp.intent_update_after = 30
        mxp.max_retry = 3
        mxp.apply()

        # add fluidos filtering attributes to own node token
        mxp.set_attr_bin("domain", self.fluidos_node_identity.Domain)
        if len(self.countries) == 1:
            mxp.set_attr_bin("country", self.countries[0])
        if len(self.cities) == 1:
            mxp.set_attr_bin("city", self.cities[0])
        if len(self.latitudes) == 1:
            mxp.set_attr_bin("latitude", self.latitudes[0])
        if len(self.longitudes) == 1:
            mxp.set_attr_bin("longitude", self.longitudes[0])
        # add fluidos node identity to own node token
        mxp.set_attr_bin(
            fluidos_node_identity_v01, self.fluidos_node_identity.model_dump_json()
        )

        np_node.add_authorize_cb(
            flavor_discovery_subject, NodeDiscovery.authorize_fluidos_nodes
        )

        logger.info("initialized fluidos node identity discovery channel")

        # we are offering resource, add flavor discovery as well
        if have_fluidos_resources:
            await np_node.add_discovery(flavor_discovery_module)

    @classmethod
    async def update_fluidos_known_clusters(self, fluidos_node_info: NodeIdentity):

        # Define the group, version, and plural of the CRD
        group = "network.fluidos.eu"  # Replace with your CRD group
        version = "v1alpha1"  # Replace with your CRD version
        plural = "knownclusters"  # Replace with your CRD plural
        namespace = "fluidos"

        async with ApiClient() as api:
            co_api = client.CustomObjectsApi(api)

            last_update_time = datetime.now()
            expiration_time = last_update_time + timedelta(seconds=120)
            create_new = False
            known_cluster = None
            try:
                known_cluster_k8s_obj = await co_api.get_namespaced_custom_object(
                    group=group,
                    version=version,
                    namespace=namespace,
                    plural=plural,
                    name=fluidos_node_info.NodeID,
                )
                known_cluster = KnownCluster.model_validate(
                    known_cluster_k8s_obj["spec"]
                )
                spec = AddressSpec(
                    address=f"{fluidos_node_info.IP}:{fluidos_node_info.Port}"
                )
                known_cluster_k8s_obj["spec"] = spec.model_dump()
                logger.info("patching existing crds known cluster  ...")
            except ApiException as api_e:
                if api_e.reason == "Not Found":
                    spec = AddressSpec(
                        address=f"{fluidos_node_info.IP}:{fluidos_node_info.Port}"
                    )
                    known_cluster_k8s_obj = {
                        "apiVersion": f"{group}/{version}",
                        "kind": "KnownCluster",  # Replace with your CRD kind
                        "metadata": {
                            "name": fluidos_node_info.NodeID,
                            "namespace": namespace,
                        },
                        "spec": spec.model_dump(),
                    }
                    logger.info("creating new crd known cluster ...")
                    create_new = True
                pass  # Debugger 12.1 bug crashes here without this statement

            try:
                if create_new:
                    await co_api.create_namespaced_custom_object(
                        group=group,
                        version=version,
                        namespace=namespace,
                        plural=plural,
                        body=known_cluster_k8s_obj,
                    )
                else:
                    await co_api.patch_namespaced_custom_object(
                        group=group,
                        version=version,
                        namespace=namespace,
                        plural=plural,
                        name=fluidos_node_info.NodeID,
                        body=known_cluster_k8s_obj,
                        _content_type="application/merge-patch+json",
                    )
                logger.info("crd known cluster written to k8s cluster...")
            except ApiException as api_e:
                logger.info(f"error while updating known cluster crd: {api_e}")

            pass  # Debugger 12.1 bug crashes here without this statement
