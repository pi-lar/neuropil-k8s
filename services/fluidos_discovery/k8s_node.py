import sys
import logging
import asyncio
from neuropil import NeuropilNode, np_token, np_subject, NeuropilException

from discovery import DiscoveryHandler

logger = logging.getLogger(__file__)


class KubernetesNode(NeuropilNode):
    """Contains methods to Use a Neuropil Node"""

    authz_callbacks = {}

    def __init__(self, human_identifier: str, port: int, hostname: str, proto: str):
        super().__init__(
            port,
            hostname,
            auto_run=False,
            n_threads=5,
            proto=proto,
        )
        self.task_queue = asyncio.Queue()
        self.discovery_modules = {}
        self.human_identifier = human_identifier

    async def add_discovery(self, handler: DiscoveryHandler):
        self.discovery_modules[handler.name] = handler
        await handler.setup(self)

    def authn_allow_all(self, node: NeuropilNode, token: np_token):
        """Bypasses the Authentication Process of Neuropil Node"""
        logger.info(
            f"{self.get_fingerprint()}: authn: {token.subject} {token.get_fingerprint()}"
        )
        return True

    def authz_allow_all(self, node: NeuropilNode, token: np_token):
        """Bypasses the Authorization Process of Neuropil Node"""
        logger.info(
            f"{self.get_fingerprint()}: authz: {token.subject} {token.get_fingerprint()}"
        )
        return True

    @classmethod
    def authorize(cls, node: NeuropilNode, token: np_token):
        logger.debug(f"authorization request for {token.subject}")
        if token.subject in cls.authz_callbacks.keys():
            return cls.authz_callbacks[token.subject](node, token)
        logger.warning(f"authorization request failed ...")
        return False

    def add_authorize_cb(self, subject: np_subject, callback):
        self.authz_callbacks[str(subject)] = callback
        self.set_authorize_cb(self.authorize)

    def acc_allow_all(self, node: NeuropilNode, token: np_token):
        """Bypasses the Accounting Process of Neuropil Node"""
        logger.info(f"{self.get_fingerprint()}: acc: {token.subject}")
        return True

    def enableAAA(self):
        """Sets the AAA Callbacks to use the Bypassing methods"""
        self.set_authenticate_cb(self.authn_allow_all)
        self.set_authorize_cb(self.authz_allow_all)
        self.set_accounting_cb(self.acc_allow_all)

    def start_node(self):
        """Starts a Neuropil Node, runs it for short period of time and then prints a info message to the logger"""
        self.enableAAA()
        self.run(0)
        logger.info(f"Neuropil Node {self.human_identifier} has been started")

    def join_node(self, connection_string):
        logger.info(
            f"Neuropil Node {self.human_identifier} requests join with {connection_string}"
        )
        super().join(connection_string)

    def print_address(self):
        """Prints the Neuropil Node Address and Subject to the logger"""
        logger.info(
            f"Neuropil Node {self.human_identifier} has the following Address and Subject: {self.get_address()}"
        )

    async def sleep(self):
        await asyncio.sleep(0.01)

    async def run(self, duration: float):
        try:
            # run neuropil loop and possible callbacks
            super(KubernetesNode, self).run(duration)

            # run python tasks
            if self.task_queue.empty():
                self.task_queue.put_nowait(self.sleep())
            async with asyncio.TaskGroup() as group:
                while not self.task_queue.empty():
                    group.create_task(self.task_queue.get_nowait())
                    self.task_queue.task_done()

        except NeuropilException as ne:
            logger.info(f"neuropil reported error {ne}")
        except asyncio.QueueEmpty:
            # can happen, not bad
            pass
